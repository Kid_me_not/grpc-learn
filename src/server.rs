#[macro_use]
extern crate prost_derive;

use futures::{future, Future, Stream};
use tokio::executor::DefaultExecutor;
use tokio::net::TcpListener;
use tower_h2::Server;
use tower_grpc::{Request, Response};

pub mod add {
    include!(concat!(env!("OUT_DIR"), "/add.rs"));
}

#[derive(Clone, Debug)]
struct S;

impl add::server::Math for S {
    type AdditionFuture = future::FutureResult<Response<add::AddResponse>, tower_grpc::Error>;

    fn addition(&mut self, req: Request<add::AddRequest>) -> Self::AdditionFuture {
        let r = req.get_ref();
        println!("REQUEST = {:?}", req);
        future::ok(Response::new(add::AddResponse {
            result: r.a + r.b
        }))
    }
}

fn main() {
    
    let new_service = add::server::MathServer::new(S);
    let h2_settings = Default::default();

    let mut h2 = Server::new(new_service, h2_settings, DefaultExecutor::current());

    let addr = &([127, 0, 0, 1], 4000).into();
    let bind = TcpListener::bind(&addr).expect("bind");

    let serve = bind.incoming()
        .for_each(move |sock| {
            
            match sock.set_nodelay(true) {
                Ok(_) => {
                    let s = h2.serve(sock);
                    tokio::spawn(s.map_err(|e| eprintln!("err: {}", e)));
                    Ok(())
                },

                e => e
            }
            
        })
        .map_err(|e| eprintln!("accept error: {}", e));
    tokio::run(serve)
}