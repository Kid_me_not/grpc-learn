#[macro_use]
extern crate prost_derive;

use futures::{Future};
use tokio::executor::DefaultExecutor;
use tokio::net::tcp::{ConnectFuture, TcpStream};
use tower_h2::client;
use tower_http::add_origin;
use tower_grpc::Request;
use tower_util::MakeService;

pub mod add;
//  {
//     include!(concat!(env!("OUT_DIR"), "/add.rs"));
// }

#[derive(Clone, Debug)]
struct C;

impl tokio_connect::Connect for C {
    type Connected = TcpStream;
    type Error = ::std::io::Error;
    type Future = ConnectFuture;

    fn connect(&self) -> Self::Future {
        // println!("HIT THIS");
        TcpStream::connect(&([127, 0, 0, 1], 4000).into())
    }
}

type MathClient<T> = add::client::Math<add_origin::AddOrigin<T>>;

fn handle_conn<T>(c: T) -> MathClient<T> {
    let conn = add_origin::Builder::new()
        .uri("http://localhost:4000")
        .build(c)
        .unwrap();
    
    add::client::Math::new(conn)
}

fn main() {

    let mut c = client::Connect::new(C, Default::default(), DefaultExecutor::current());

    let c_ = c.make_service(())
        .map(handle_conn)
        .and_then(|mut client| {
            client
                .addition(Request::new(add::AddRequest {
                    a: 3,
                    b: 4
                }))
                .map_err(|e| panic!("gRPC Request failed; err={:?}", e))
        })
        .and_then(|res| {
            println!("RESPONSE = {:?}", res.get_ref().result);
            Ok(())
        })
        .map_err(|e| {
            println!("ERR = {:?}", e)
        });
    tokio::run(c_);
    println!("DONE")
}