#[derive(Clone, PartialEq, Message)]
pub struct AddRequest {
    #[prost(uint64, tag="1")]
    pub a: u64,
    #[prost(uint64, tag="2")]
    pub b: u64,
}
#[derive(Clone, PartialEq, Message)]
pub struct AddResponse {
    #[prost(uint64, tag="1")]
    pub result: u64,
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{AddRequest, AddResponse};

    #[derive(Debug, Clone)]
    pub struct Math<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> Math<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Error<T::Error>>
        where T: tower::HttpService<R>,
        {
            self.inner.poll_ready()
        }

        pub fn addition<R>(&mut self, request: grpc::Request<AddRequest>) -> grpc::unary::ResponseFuture<AddResponse, T::Future, T::ResponseBody>
        where T: tower::HttpService<R>,
              T::ResponseBody: grpc::Body,
              grpc::unary::Once<AddRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/add.Math/Addition");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{AddRequest, AddResponse};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait Math: Clone {
        type AdditionFuture: futures::Future<Item = grpc::Response<AddResponse>, Error = grpc::Error>;

        fn addition(&mut self, request: grpc::Request<AddRequest>) -> Self::AdditionFuture;
    }

    #[derive(Debug, Clone)]
    pub struct MathServer<T> {
        math: T,
    }

    impl<T> MathServer<T>
    where T: Math,
    {
        pub fn new(math: T) -> Self {
            Self { math }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for MathServer<T>
    where T: Math,
    {
        type Response = http::Response<math::ResponseBody<T>>;
        type Error = h2::Error;
        type Future = math::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::math::Kind::*;

            match request.uri().path() {
                "/add.Math/Addition" => {
                    let service = math::methods::Addition(self.math.clone());
                    let response = grpc::Grpc::unary(service, request);
                    math::ResponseFuture { kind: Ok(Addition(response)) }
                }
                _ => {
                    math::ResponseFuture { kind: Err(grpc::Status::with_code(grpc::Code::Unimplemented)) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for MathServer<T>
    where T: Math,
    {
        type Response = Self;
        type Error = h2::Error;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_h2::RecvBody>> for MathServer<T>
    where T: Math,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_h2::RecvBody>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::new(Box::new(b)));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod math {
        use ::tower_grpc::codegen::server::*;
        use super::Math;
        use super::super::AddRequest;

        pub struct ResponseFuture<T>
        where T: Math,
        {
            pub(super) kind: Result<Kind<
                grpc::unary::ResponseFuture<methods::Addition<T>, grpc::BoxBody, AddRequest>,
            >, grpc::Status>,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: Math,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = h2::Error;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Ok(Addition(ref mut fut)) => {
                        let response = try_ready!(fut.poll());
                        let (head, body) = response.into_parts();
                        let body = ResponseBody { kind: Ok(Addition(body)) };
                        let response = http::Response::from_parts(head, body);
                        Ok(response.into())
                    }
                    Err(ref status) => {
                        let body = ResponseBody { kind: Err(status.clone()) };
                        Ok(grpc::Response::new(body).into_http().into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: Math,
        {
            pub(super) kind: Result<Kind<
                grpc::Encode<grpc::unary::Once<<methods::Addition<T> as grpc::UnaryService<AddRequest>>::Response>>,
            >, grpc::Status>,
        }

        impl<T> grpc::Body for ResponseBody<T>
        where T: Math,
        {
            type Data = bytes::Bytes;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    Ok(Addition(ref v)) => v.is_end_stream(),
                    Err(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, grpc::Error> {
                use self::Kind::*;

                match self.kind {
                    Ok(Addition(ref mut v)) => v.poll_data(),
                    Err(_) => Ok(None.into()),
                }
            }

            fn poll_metadata(&mut self) -> futures::Poll<Option<http::HeaderMap>, grpc::Error> {
                use self::Kind::*;

                match self.kind {
                    Ok(Addition(ref mut v)) => v.poll_metadata(),
                    Err(ref status) => {
                        status.to_header_map().map(Some).map(Into::into)
                    }
                }
            }
        }

        impl<T> tower_h2::Body for ResponseBody<T>
        where T: Math,
        {
            type Data = bytes::Bytes;

            fn is_end_stream(&self) -> bool {
                grpc::Body::is_end_stream(self)
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, h2::Error> {
                grpc::Body::poll_data(self)
                    .map_err(h2::Error::from)
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, h2::Error> {
                grpc::Body::poll_metadata(self)
                    .map_err(h2::Error::from)
            }
        }

        #[derive(Debug, Clone)]
        pub(super) enum Kind<Addition> {
            Addition(Addition),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{Math, AddRequest, AddResponse};

            pub struct Addition<T>(pub T);

            impl<T> tower::Service<grpc::Request<AddRequest>> for Addition<T>
            where T: Math,
            {
                type Response = grpc::Response<AddResponse>;
                type Error = grpc::Error;
                type Future = T::AdditionFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<AddRequest>) -> Self::Future {
                    self.0.addition(request)
                }
            }
        }
    }
}
