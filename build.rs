fn main() {
    // prost_build::compile_protos(&["src/items.proto"], &["src/"]).unwrap();
    tower_grpc_build::Config::new()
        .enable_server(true)
        .enable_client(true)
        .build(&["proto/add.proto"], &["proto/"])
        .unwrap()
}
